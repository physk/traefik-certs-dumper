while ! [ -e /data/acme.json ] || ! [ `jq ".Certificates | length" /data/acme.json` != 0 ]; do
  sleep 1;
done
traefik-certs-dumper file --watch --source /data/acme.json --dest /certs