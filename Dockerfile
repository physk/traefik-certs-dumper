FROM ldez/traefik-certs-dumper:v2.8.1

COPY run.sh /

RUN apk update && \
    apk add jq && \
    chmod +x /run.sh



VOLUME "/data"
VOLUME "/certs"

ENTRYPOINT ["/run.sh"]